<?php

namespace Drupal\andi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AndiSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'andi.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'andi_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('andi.settings');

    // Load all roles
    $role_options = user_role_names();

    $form['andi_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles that can view ANDI link'),
      '#default_value' => $config->get('andi_roles') ?: [],
      '#options' => $role_options,
      '#description' => $this->t('Select roles that should be able to see the ANDI link on the toolbar.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('andi.settings')
      ->set('andi_roles', $form_state->getValue('andi_roles'))
      ->save();
  }
}
